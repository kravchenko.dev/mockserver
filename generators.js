const fs = require("fs");
const { faker } = require("@faker-js/faker");
const db = require("./db.json");

const generateExam = (_, index) => {
  return {
    id: faker.datatype.uuid(),
    title: `Exam ${index + 1}`,
    author: null,
    description: faker.lorem.lines(1),
    frequency: "daily",
    startDate: faker.date.past(),
    tests: [
      "TYPE_TEMPERATURE",
      "TYPE_PULSEOX",
      "TYPE_EYES",
      "TYPE_EARS",
      "TYPE_NOSE",
      "TYPE_THROAT",
      "TYPE_HEART",
      "TYPE_LUNGS",
      "TYPE_SKIN",
      "TYPE_BLOODPRESURE",
    ],
  };
};

const generateExamData = (_, index) => {
  return {
    id: index + 1,
    title: `Exam ${index + 1}`,
    author: null,
    description: faker.lorem.lines(1),
    createdAt: faker.date.past(),
    assignedCount: Number(faker.random.numeric(2)),
    takenCount: Number(faker.random.numeric(3)),
    frequency: "daily",
    tests: [
      "TYPE_TEMPERATURE",
      "TYPE_PULSEOX",
      "TYPE_EYES",
      "TYPE_EARS",
      "TYPE_NOSE",
      "TYPE_THROAT",
      "TYPE_HEART",
      "TYPE_LUNGS",
      "TYPE_SKIN",
      "TYPE_BLOODPRESURE",
    ],
  };
};

const _generateExamResults = (_, index) => {
  return {
    id: faker.datatype.uuid(),
    exam: generateExam(_, index),
    weight: faker.random.numeric(3),
    height: faker.random.numeric(3),
    date: faker.date.past(),
    note: faker.lorem.lines(Math.random() * 10),
    tests: [
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_TEMPERATURE",
        temperature: faker.random.numeric(2),
      },
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_PULSEOX",
        bloodOxigen: faker.random.numeric(2),
        bloodPresure: {
          systolic: faker.random.numeric(3),
          diastolic: faker.random.numeric(2),
        },
        heartRate: faker.random.numeric(2),
        perfusionIndex: faker.random.numeric(1),
      },
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_BLOODPRESURE",
        bloodPressure: {
          systolic: faker.random.numeric(3),
          diastolic: faker.random.numeric(2),
        },
      },
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_THROAT",
        locations: [0],
        samples: [
          { data: faker.image.abstract(), location: 0, mimeType: "image/png" },
        ],
      },
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_EYES",
        locations: [0, 1],
        samples: [
          { data: faker.image.abstract(), location: 0, mimeType: "image/png" },
          { data: faker.image.abstract(), location: 1, mimeType: "image/png" },
        ],
      },
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_EARS",
        locations: [0, 1],
        samples: [
          { data: faker.image.abstract(), location: 0, mimeType: "image/png" },
          { data: faker.image.abstract(), location: 1, mimeType: "image/png" },
        ],
      },
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_NOSE",
        locations: [0, 1],
        samples: [
          { data: faker.image.abstract(), location: 0, mimeType: "image/png" },
          { data: faker.image.abstract(), location: 1, mimeType: "image/png" },
        ],
      },
      {
        id: faker.datatype.uuid(),
        kind: "TYPE_SKIN",
        locations: [],
        samples: [
          { data: faker.image.abstract(), location: 0, mimeType: "image/png" },
          { data: faker.image.abstract(), location: 1, mimeType: "image/png" },
          { data: faker.image.abstract(), location: 2, mimeType: "image/png" },
          { data: faker.image.abstract(), location: 3, mimeType: "image/png" },
        ],
      },
    ],
  };
};

const generateExamResults = () => {
  return {
    id: faker.datatype.uuid(),
    name: "Exam name",
    temp: faker.random.numeric(2),
    spo2: faker.random.numeric(2),
    prbpm: faker.random.numeric(2),
    pi: faker.random.numeric(1),
    rr: `${faker.random.numeric(2)} - ${faker.random.numeric(2)}`,
    sys: faker.random.numeric(3),
    dia: faker.random.numeric(2),
    weight: faker.random.numeric(3),
    date: faker.datatype.datetime(), // "$datatype.datetime",
    throat: [faker.image.abstract()],
    eyes: [faker.image.abstract(), faker.image.abstract()],
    ears: [faker.image.abstract(), faker.image.abstract()],
    nose: [faker.image.abstract(), faker.image.abstract()],
    skin: [
      faker.image.abstract(),
      faker.image.abstract(),
      faker.image.abstract(),
    ],
    note: faker.lorem.lines(3),
  };
};

const generatePatientResults = () => {
  return {
    id: faker.datatype.uuid(),
    avatar: faker.image.avatar(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    age: faker.random.numeric(2),
    gender: faker.name.sex(),
    city: faker.address.city(),
    state: faker.address.stateAbbr(),
    insurance: faker.company.name(),
    status: "STATUS_ACTIVE",
    dob: faker.date.birthdate(),
    address: faker.address.secondaryAddress(),
    pharmacy: faker.name.jobArea(),
    phone: faker.phone.number(),
    weight: faker.random.numeric(3),
    height: faker.random.numeric(2),
    lastActivity: faker.date.past(),
  };
};

const generators = {
  patients: generatePatientResults,
  "exam-results": _generateExamResults,
  exams: generateExamData,
};

const generateData = () => {
  const nextDb = { ...db };
  Object.entries(generators).forEach(([path, fn]) => {
    const data = [...new Array(20)].map(fn);
    nextDb[path] = data;
  });

  fs.writeFileSync("./db.json", JSON.stringify(nextDb), (err) => {
    if (err) {
      return console.error("Error", err);
    }

    return console.log("Success");
  });
};

module.exports = generateData();
