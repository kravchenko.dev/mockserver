const express = require("express");
const cors = require("cors");
const { flow } = require("lodash");
const db = require("./db.json");

const app = express();

app.use(cors());

const port = 3000;

const getPaginatedResponse = (path) => ({
  result: {
    data: db[path],
    limits: 20,
    page: 1,
    total: 90,
  },
});

const delayed = (cb, delay = 800) =>
  setTimeout(() => {
    cb();
  }, delay);

const getResource = (path, id) => {
  return { result: db[path].find((resource) => resource.id === id) };
};

app.get("/patients", (req, res) => {
  res.json(getPaginatedResponse("patients"));
});

app.get("/patients/:id", (req, res) => {
  res.json(getResource("patients", req.params.id));
});

app.get("/exam-results", (req, res) => {
  delayed(() => res.json(getPaginatedResponse("exam-results")));
});

app.get("/exam-results/:id", (req, res) => {
  delayed(() => res.json(getResource("exam-results", req.params.id)));
});

app.get("/exams", (req, res) => {
  delayed(() => res.json(getPaginatedResponse("exams")));
});

app.get("/exams/:id", (req, res) => {
  delayed(() => res.json(getResource("exams", Number(req.params.id))));
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
